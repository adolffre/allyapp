//
//  MapViewSegmentCVCell.swift
//  AllyApp
//
//  Created by A. J. on 17/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit

class MapViewSegmentCVCell: UICollectionViewCell {

    @IBOutlet weak var imgTravelMode: UIImageView!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblStops: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblQtdStops: UILabel!
    @IBOutlet weak var lblName: UILabel!
}