//
//  Segment.swift
//  AllyApp
//
//  Created by A. J. on 14/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit

class Segment: NSObject {
    var name      : String!
    var numStops  : Int!
    var travelMode: String!
    var descrip   : String!
    var color     : String!
    var iconUrl   : String!
    var polyline  : String!
    var stops     : Array<Stop>!
    
    var firstStop : Stop{
        get{
            return stops.first!
        }
    }
    var lastStop : Stop{
        get{
            return stops.last!
        }
    }

    class func segmentsFromArray(arraySegments: Array<[String: AnyObject] >) -> Array<Segment>{

        var segments = Array<Segment>()
       
        for segmentDic in arraySegments  {
            let segment = Segment()
            segment.name       = segmentDic["name"] as? String
            segment.numStops   = segmentDic["num_stops"] as! Int
            segment.travelMode = segmentDic["travel_mode"] as? String
            segment.descrip    = segmentDic["descripton"] as? String
            segment.color      = segmentDic["color"] as? String
            segment.iconUrl    = segmentDic["icon_url"] as? String
            segment.polyline   = segmentDic["polyline"] as? String
            segment.stops      = Stop.stopsFromArray(segmentDic["stops"] as! Array<[String: AnyObject] >)
            segments.append(segment)
        }

        return segments
        
    }
      
}
