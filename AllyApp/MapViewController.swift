//
//  MapViewController.swift
//  AllyApp
//
//  Created by A. J. on 14/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit
import GoogleMaps

class MapViewController: UIViewController , CLLocationManagerDelegate, RoutesViewControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate{
    
    //MARK: Properties/IBOutlets
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var btnGo: UIButton!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var navItemBtnClear: UIBarButtonItem!
    @IBOutlet weak var pageControl: UIPageControl!

    @IBOutlet weak var viewPageControl: UIView!
    let locationManager = CLLocationManager()
    var routes : Array<Route>!
    var allStops : Array<Stop>!
    var segments : Array<Segment>!
    
    var selectedRoute : Route!{
        willSet (newRoute)
        {
            self.selectedRoute = newRoute
            self.mapView.clear()
            
        }
        didSet {

            if self.selectedRoute != nil{
                self.navItemBtnClear.enabled = true
                let firstSeg = self.selectedRoute.segments.first
                let lastSeg  = self.selectedRoute.segments.last
                let firstStop = firstSeg!.firstStop
                let lastStop  = lastSeg!.lastStop
                
                let firstLocation =  CLLocationCoordinate2DMake(firstStop.lat.doubleValue, firstStop.lng.doubleValue)
                
                let lastLocation  =  CLLocationCoordinate2DMake(lastStop.lat.doubleValue, lastStop.lng.doubleValue)
                
                let marker1 = GMSMarker(position: firstLocation)

                marker1.title = firstStop.name != nil ? "From Here - "+"\(firstStop.name)": "From Here"
                marker1.map = self.mapView
                marker1.icon = GMSMarker.markerImageWithColor(UIColor.blueColor())
                
                let marker2 = GMSMarker(position: lastLocation)
                marker2.title = "To Here - "+"\(lastStop.name)"

                marker2.map = self.mapView
                marker2.icon = GMSMarker.markerImageWithColor(UIColor.greenColor())
                
                var stops = Array<Stop>()
                
                for segment in self.selectedRoute.segments
                {
                    for stop in segment.stops
                    {
                        stops.append(stop)

                        if ((stop.dateTime != firstStop.dateTime) && (stop.dateTime != lastStop.dateTime))
                        {
                            let position = CLLocationCoordinate2DMake(stop.lat.doubleValue,stop.lng.doubleValue)
                            let mark = GMSMarker(position: position)
                            mark.icon = GMSMarker.markerImageWithColor(UIColor.blackColor())
                            mark.flat = true
                            mark.title = stop.name
                            mark.map = mapView
                        }
                    }

                    let path = GMSMutablePath(fromEncodedPath: segment.polyline)
                    
                    let polyline = GMSPolyline(path: path)
                    polyline.strokeColor = UIColor(hexString: segment.color)
                    polyline.strokeWidth = 5.0
                    polyline.map = self.mapView
                    
                    
                }
                self.segments = self.selectedRoute.clearSegments()
                self.allStops = stops
                mapView.camera = GMSCameraPosition(target: firstLocation, zoom: 13, bearing: 0, viewingAngle: 0)
                
                locationManager.stopUpdatingLocation()
            }
            else{
                self.navItemBtnClear.enabled = false    
            }
            self.collectionView.reloadData()
            self.collectionView.setContentOffset(CGPointZero, animated: true)
        }
    }
    
    //MARK: Lifecycle View
    override func viewDidLoad() {
        super.viewDidLoad()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()

    }

    //MARK: TextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool{
        if textField == self.txtFrom{
            self.txtTo.becomeFirstResponder()
        }else{
            self.txtTo.endEditing(true)
            self.btnGoPressed(self.btnGo)
        }
        return true
    }
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.txtFrom.endEditing(true)
        self.txtTo.endEditing(true)
    }
    
    //MARK: LocationManager
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if status == .AuthorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
            
            mapView.myLocationEnabled = true

        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first  {
            
            mapView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            
            locationManager.stopUpdatingLocation()
        }
        
    }
    //MARK: Actions
    @IBAction func btnGoPressed(sender: AnyObject)
    {
        let dicFromJson = self.jsonDataArray
        self.routes = Route.routesFromJson(dicFromJson)
        self.txtFrom.text = "Torstraße 105-107, 10119 Berlin, Germany"
        self.txtTo.text   = "Leipziger Platz 7, 10117 Berlin, Germany"
        self.openClosePressed(self)
        self.performSegueWithIdentifier("idPushRoutes", sender: self)
        
    }
    
    @IBAction func btnClearSearchPressed(sender: AnyObject) {
        self.selectedRoute = nil
        self.routes = nil
        self.txtFrom.text = ""
        self.txtTo.text = ""
        self.openSearchView()
        
    }
    @IBAction func openClosePressed(sender: AnyObject) {

        if self.searchView.frame.origin.y == 0
        {
            self.closeSearchView()
        }
        else
        {
            if self.selectedRoute != nil{
                performSegueWithIdentifier("idPushRoutes", sender: self)
                return
            }
            self.openSearchView()
        }
        
    }
    func openSearchView(){
        UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseOut, animations: {
            let frame = self.searchView.frame
            self.searchView.translatesAutoresizingMaskIntoConstraints = true
            self.searchView.frame = frame
            self.searchView.frame.origin =  CGPointMake(self.searchView.frame.origin.x, 0)
            
            }, completion: { finished in
        })

    }
    func closeSearchView(){
        UIView.animateWithDuration(0.5, delay: 0, options: .CurveEaseOut, animations: {
            let frame = self.searchView.frame
            self.searchView.translatesAutoresizingMaskIntoConstraints = true
            self.searchView.frame = frame
            self.searchView.frame.origin =  CGPointMake(self.searchView.frame.origin.x, -175)
            
            }, completion: { finished in
        })
    }
    //MARK: Custom Methods
    var jsonDataArray: [String: AnyObject] {
        get {
            var dicJson = [String: AnyObject] ()
            
            let filePathJson    = NSBundle.mainBundle().pathForResource("data", ofType: "json")
            
            do{
                
                let myJson          = try String(contentsOfFile: filePathJson!, encoding: NSUTF8StringEncoding)
                dicJson = try NSJSONSerialization.JSONObjectWithData(myJson.dataUsingEncoding(NSUTF8StringEncoding)!, options: .AllowFragments) as! [String: AnyObject]
                
                return dicJson
            }
                
            catch {
                let alert = UIAlertController(title: "Error", message: "Error getting data of JSON).", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            return dicJson
        }
    }
    func configurePageControl() {
        self.pageControl.numberOfPages = self.segments.count
        self.pageControl.currentPage = 0
      
    }
    //MARK: RoutesViewControllerDelegate
    func didSelectRoute(viewController:RoutesViewController  , route:Route){
        self.selectedRoute = route
    }
    
    //MARK: Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "idPushRoutes"{
            
            let vc = segue.destinationViewController as! RoutesViewController
            vc.routes   = self.routes
            vc.addrFrom = self.txtFrom.text
            vc.addrTo   = self.txtTo.text
            vc.delegate = self
           }
    
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if (self.selectedRoute != nil){
            self.collectionView.hidden = false
            self.viewPageControl.hidden = false
            self.configurePageControl()
            return self.segments.count
        }
        self.collectionView.hidden = true
        self.viewPageControl.hidden = true
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("segmentsCell", forIndexPath: indexPath) as! MapViewSegmentCVCell
        let seg = segments[indexPath.row]
        let lastStop = seg.lastStop
        
        cell.lblName.hidden     = true
        cell.lblStops.hidden    = true
        cell.lblQtdStops.hidden = true

        if let name = seg.name {
            cell.lblName.text = name
            cell.lblName.hidden = false
        }
        if seg.numStops != 0 {
            cell.lblQtdStops.text   =  "\(seg.numStops)"
            cell.lblStops.hidden    = false
            cell.lblQtdStops.hidden = false

        }
        cell.lblTo.text = lastStop.name
        let dic = self.selectedRoute.dataFromSegmentsAtIndex(indexPath.row)
        let arr = dic[0]
        
        cell.imgTravelMode.image = arr?[0] as? UIImage
        let minutes = arr?[1] as? Int
        cell.lblTime.text        = "\(minutes!)"+"m"
        return cell
        
        
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        let cell  = self.collectionView.visibleCells().first!

        let indexPath  = self.collectionView.indexPathForCell(cell)
        self.pageControl.currentPage = indexPath!.row
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSizeMake((UIScreen.mainScreen().bounds.width),138);
    }
    
}


