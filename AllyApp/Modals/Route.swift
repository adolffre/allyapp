//
//  Route.swift
//  AllyApp
//
//  Created by A. J. on 14/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit

class Route: NSObject {
    var type      : String!
    var provider  : String!
    var price     : Price!
    var segments  : Array<Segment>!
    var properties: [String: AnyObject]!
    
    //MARK: Custom methods
    class func routesFromJson(dic: [String: AnyObject]) -> Array<Route>{
        
        let dicRoutes = dic["routes"] as! Array<[String: AnyObject]>
        var routes = Array<Route>()
        
        for routeDic in dicRoutes {
            let route = Route()
            route.type       = routeDic["type"]                   as? String
            route.provider   = routeDic["provider"]               as? String
            let price        = Price(dicPrice: (routeDic["price"] as AnyObject?)!)
            route.price      = price
            let segments     = Segment.segmentsFromArray(routeDic["segments"] as! Array<[String: AnyObject]>)
            
            route.segments   = segments
            route.properties = routeDic["properties"]           as? [String: AnyObject]
            routes.append(route)
        }
        
        return routes
    
    }
    func dataFromSegmentsAtIndex(index: Int?) -> [Int: AnyObject] {
        var segs = Array<Segment>()
        
        if index != nil{
            let cleanSegs = self.clearSegments()
            segs.append(cleanSegs[index!])
            
        }
        else
        {
            segs = self.segments 
        }
        var dic = [Int: AnyObject]()
        var index = 0
        for segment in segs 
        {
            if (segment.polyline != nil)
            {
                var array = [AnyObject]()
                
                switch segment.travelMode {
                    
                case "walking":
                    
                    array.insert(UIImage(named:"g_walk")!, atIndex: 0)
                    
                case "cycling":
                    if self.type == "bike_sharing"
                    {
                        array.insert(UIImage(named:"g_bike_shared")!, atIndex: 0)
                    }
                    else
                    {
                        array.insert(UIImage(named:"g_bike")!, atIndex: 0)
                    }
                case "driving":
                    
                    if self.type == "car_sharing"
                    {
                        array.insert(UIImage(named:"g_uber")!, atIndex: 0)
                    }
                    else
                    {
                        array.insert(UIImage(named:"g_taxi")!, atIndex: 0)
                    }
                case "subway":
                    array.insert(UIImage(named:"g_subway")!, atIndex: 0)
                default:
                    array.insert(UIImage(named:"g_bus")!, atIndex: 0)
                    
                }
                
                let firstStop = segment.firstStop
                let lastStop  = segment.lastStop
                
                let date1 = firstStop.dateTime.dateWithFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                
                let date2 = lastStop.dateTime.dateWithFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                
                let minutesElapsed = date1.minutesElapsedSinceDate(date2)
                
                
                array.insert(minutesElapsed , atIndex: 1)
                dic[index] = array
                index++
            }
            
        }
        return dic
    }
    func clearSegments() -> Array<Segment>
    {
        var segments = Array<Segment>()
        
        for segment in self.segments
        {
            if (segment.polyline != nil)
            {
             segments.append(segment)
            }
        }
        return segments
    }

    
}
