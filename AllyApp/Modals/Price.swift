//
//  Price.swift
//  AllyApp
//
//  Created by A. J. on 14/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit

class Price: NSObject {
    var currency: String!
    var amount  : AnyObject!
    
    init (dicPrice : AnyObject) {
        if dicPrice.isKindOfClass(NSDictionary){
            self.currency = dicPrice.objectForKey("currency") as! String
            self.amount   = dicPrice.objectForKey("amount")    as AnyObject?
        }
    }
    
    
}
