//
//  Stop.swift
//  AllyApp
//
//  Created by A. J. on 14/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit

class Stop: NSObject {
    var lat     : NSNumber!
    var lng     : NSNumber!
    var dateTime: String!
    var name    : String!

    class func stopsFromArray(arrayStops: Array<[String: AnyObject] >) -> Array<Stop>{
        
        var stops = Array<Stop>()
        
        for stopDic in arrayStops  {
            let stop = Stop()
            stop.lat       = stopDic["lat"] as? NSNumber
            stop.lng       = stopDic["lng"] as? NSNumber
            stop.dateTime  = stopDic["datetime"] as? String
            stop.name      = stopDic["name"] as? String
            stops.append(stop)
        }
        
        return stops
        
    }
}
