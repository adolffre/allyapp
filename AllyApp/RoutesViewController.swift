//
//  RoutesViewController.swift
//  AllyApp
//
//  Created by A. J. on 15/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit

protocol RoutesViewControllerDelegate{
    func didSelectRoute(viewController:RoutesViewController  , route:Route)
}
class RoutesViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    //MARK: Properties/IBOutlets
    var routes   : Array<Route>!
    var addrFrom : String!
    var addrTo   : String!
    var delegate :RoutesViewControllerDelegate?
    
    var selectedRoute : Route!

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var txtTo: UITextField!
    //MARK: Lifecycle View
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        txtFrom.text = addrFrom
        txtTo.text   = addrTo
    }
    
    
    //MARK: Tableview DataSource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.routes.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("routesMapCell", forIndexPath: indexPath) as UITableViewCell
        
        let route = self.routes[indexPath.row]

        var price = "-"
        if route.price.currency != nil{
            price = route.price.amount.doubleValue.toUIString()
        }
        
        let viewSegments: UIView?  = self.view.viewWithTag(1001)
        let lblPrice    : UILabel? = self.view.viewWithTag(1002) as? UILabel
        let lblTotalTime: UILabel? = self.view.viewWithTag(1003) as? UILabel
        
        let dic = route.dataFromSegmentsAtIndex(nil)
        var tagImg  = 2001
        var tagTime = 3001
        
        
        for view in viewSegments!.subviews as [UIView]
        {
            view.hidden = true
        }
        var allTime = 0
        for var i=0 ; i<dic.keys.count ; i++
        {
            let arr = dic[i]
            let img   = viewSegments?.viewWithTag(tagImg) as! UIImageView
            let time  = viewSegments?.viewWithTag(tagTime) as! UILabel
            img.image = arr?[0] as? UIImage
            let minutes = arr?[1] as? Int
            time.text = "\(minutes!)"+"m"
            tagImg++
            tagTime++
            img.hidden = false
            time.hidden = false
            allTime += minutes!
        }
        lblPrice?.text = price
        lblTotalTime?.text = "\(allTime)"+"min"
        return cell
        
    }
    //MARK: Tableview Delegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selectedRoute = self.routes[indexPath.row]
        self.delegate?.didSelectRoute(self, route: self.selectedRoute)
        self.navigationController?.popViewControllerAnimated(true)
    }

}