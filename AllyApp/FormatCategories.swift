//
//  FormatCategories.swift
//  AllyApp
//
//  Created by A. J. on 14/09/15.
//  Copyright © 2015 AJ. All rights reserved.
//

import UIKit
import Foundation

extension UIColor {
    
    convenience init( hexString: String) {
        // Trim leading '#' if needed
        var cleanedHexString = hexString
        if hexString.hasPrefix("#") {
            
            cleanedHexString = String(hexString.characters.dropFirst())
        }
        
        // String -> UInt32
        var rgbValue: UInt32 = 0
        NSScanner(string: cleanedHexString).scanHexInt(&rgbValue)
        
        // UInt32 -> R,G,B
        let red = CGFloat((rgbValue >> 16) & 0xff) / 255.0
        let green = CGFloat((rgbValue >> 08) & 0xff) / 255.0
        let blue = CGFloat((rgbValue >> 00) & 0xff) / 255.0
        
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
}
extension Double {
    func toUIString() -> String{
        let amount = self / 100
        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "es_ES")
        
        return formatter.stringFromNumber(amount)!
    }
    
}
extension String{
    func dateWithFormat(string : String) -> NSDate
    {
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = string as String
        return dateFormatter.dateFromString(self as String)!
        
    }
}
extension NSDate{
    func minutesElapsedSinceDate(date : NSDate) -> Double
    {
        let distanceBetweenDates = date.timeIntervalSinceDate(self)
        let secondsInAnMinute = 60.0;
        return  distanceBetweenDates / secondsInAnMinute;
    }
}